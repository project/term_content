Term Content
--------------------------

Term content is a simple administration UI to manage (sort/filter) Taxonomy term's content

Why Term Content ?
--------------------------

There is no any way to customize the term content's order in drupal, most of the times we are using nodequeue module to order the nodes. It is very hard to manage the content in both nodequeue and taxonomy, This module might solve the problem. we can easily customize the node's order based on the terms and also we could list the nodes in views based on the custom orders.

How to use the module ?
--------------------------

1. Download and enable the module,
2. Go to any one of the vocabulary list term page, you can see the "content" link in the operation column, just click the link.
3. It would show the term's content in a table, you can customize the order as your wish.

How to list the content in views as the custom order ?
-------------------------------------------------------

1. create view with the term filter, so it would list only the term's content,
2. Add a sorting option with the term, you can see the Asc, Desc, Term Content Sorting option the form, just choose the Term Content sorting option.
3. that is enough, save the views, it would list the nodes as your custom order.
