<?php

/**
 * @file
 * Alter the views data.
 */

/**
 * Implements hook_field_views_data_alter().
 */
function term_content_field_views_data_alter(&$result, $field, $module) {
  if ($field['type'] == 'taxonomy_term_reference') {
    foreach ($result as $table_name => $table_data) {
      if (isset($table_data[$field['field_name']]['field'])) {
        $result[$table_name][$field['field_name'] . '_tid']['sort']['handler'] = 'term_content_handler_sort_content_order';
      }
    }
  }
}
