<?php

/**
 * @file
 * Extend the views handler sort for the term content filter.
 */

/**
 * Extends views handler sort.
 */
class term_content_handler_sort_content_order extends views_handler_sort {

  /**
   * Called to add the sort to a query.
   */
  function query() {
    $this->ensure_my_table();
    if ($this->options['order'] == 'CUSTOM') {
      $content_weight = _term_content_get_content_order_by_tid($this->view->filter[$this->field]->value);
      if (!empty($content_weight)) {
        $join = new views_join();
        $join->table = 'term_content_content';
        $join->field = 'nid';
        $join->left_table = 'node';
        $join->left_field = 'nid';
        $join->type = 'INNER';
        $this->query->add_relationship('term_content_content', $join, 'node');
        $this->query->add_where(1, 'term_content_content.tid', $this->view->filter[$this->field]->value, 'IN');
        $this->query->add_orderby('term_content_content', 'weight', 'ASC');
      }
    }
    else {
      // Add the field.
      $this->query->add_orderby($this->table_alias, $this->real_field, $this->options['order']);
    }
  }

  /**
   * Provide a list of options for the default sort form.
   */
  function sort_options() {
    return array(
      'ASC' => t('Sort ascending'),
      'DESC' => t('Sort descending'),
      'CUSTOM' => t('Term content list'),
    );
  }

  /**
   * To check the field is available for the sorting by term content.
   */
  function sort_validate(&$form, &$form_state) {
    $values = $form_state['values'];
    if ($values['options']['order'] == CUSTOM) {
      if (!isset($this->view->display['default']->display_options['filters'][$this->field])) {
        form_set_error('order', t('you must add filter for this field'));
      }
      elseif (count($this->view->display['default']->display_options['filters'][$this->field]['value']) != 1) {
        form_set_error('order', t('You should select only one term in filter option for the field'));
      }
      else {

      }
    }
  }

}
